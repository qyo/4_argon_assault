﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField] float controlSpeed = 40f;
    [Tooltip("In m")] [SerializeField] float xRange = 16f;
    [Tooltip("In m")] [SerializeField] float yRange = 6.85f;

    [Header("Screen-position Based")]
    [SerializeField] float positionPitchFactor = -1.48f;    
    [SerializeField] float positionYawFactor = 2f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchFactor = -20f;
    [SerializeField] float controlRollFactor = -20f;

    float xThrow, yThrow;
    bool isControlEnabled = true;

    private void OnPlayerDeath() //Called by string reference
    {
        print("Message recieved");
        isControlEnabled = false;
    }

    void Update()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation(); 
        }
    }

    private void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");

        float xOffsetThisFrame = xThrow * controlSpeed * Time.deltaTime;
        float yOffsetThisFrame = yThrow * controlSpeed * Time.deltaTime;

        float rawXPos = transform.localPosition.x + xOffsetThisFrame;
        float rawYPos = transform.localPosition.y + yOffsetThisFrame;

        float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);
        float clampedYPos = Mathf.Clamp(rawYPos, -yRange, yRange);

        transform.localPosition = new Vector3(clampedXPos,
            clampedYPos, transform.localPosition.z);
    }

    private void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControlThrow = yThrow * controlPitchFactor;

        float pitch = pitchDueToPosition + pitchDueToControlThrow;
        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }
}
